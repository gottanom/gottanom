<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";

	//include 'accountinfomenu.php';
	
	$outputtext .= "
		<div id='main_right' class='col span_3_of_4'>
			<div id='top'>
				<h1>
					Change Your Password
				</h1>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
				<form id='change_password_form' onSubmit='submitForm(this,".'"updatepassword"'.");return false' method='post' class='updateinfo_form' autocomplete='off'>
	
					<table id='registrationtable'>
					
					<tr>
					<th colspan='2' class='center'>New Password</th>
					</tr>
	
					<tr>
					<th colspan='2' class='optional' id='password_test'><input type='password' id='password' name='password' onkeyup='testPassword(this,{".'"required"'.":false})' placeholder='Password' size='30' /></th>
					</tr>
	
					<tr>
					<th colspan='2' class='optional' id='confirm_password_test'><input type='password' id='confirm_password' name='confirm_password' onkeyup='testConfirmPassword(this,{".'"required"'.":false})' placeholder='Confirm Password' size='30' /></th>
					</tr>
					
					<tr>
					<th colspan='2' class='center'><hr/></th>
					</tr>
					
					<tr>
					<th colspan='2' class='test' id='current_password_test'><input type='password' id='current_password' name='current_password' onkeyup='testPassword(this)' placeholder='Current Password' size='30' /></th>
					</tr>
	
					<tr>
					<th colspan='2' id='registerbutton_test' class='center'><input type='submit' id='registerbutton' value='Update Info' title='Disabled' disabled='true' /></th>
					</tr>
	
					</table>
	
				</form>
			<center>
			<script>
				testForm('#change_password_form');
				checkForm('#change_password_form');
			</script>
		</div>
	</div>
	";
	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    //echo $outputtext;
?>