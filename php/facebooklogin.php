<?php
  
  include_once "connecttowrite.php";

  require '../vendor/autoload.php';

  use Facebook\FacebookSession;
  use Facebook\FacebookRequest;
  use Facebook\FacebookRequestException;

  $token = $_POST['token'];
  FacebookSession::setDefaultApplication('894580503899773', 'b7ea9d916caea560851cf8dff2d195b5');
  $session = new FacebookSession($token);
  $request = new FacebookRequest($session, 'GET', '/me');
  $responseArray = $request->execute()->getGraphObject()->asArray();
  //id email first_name gender last_name link locale name timezone updated_time verified

  if(array_key_exists('email', $responseArray)) {
    $email = $responseArray['email'];
  }

  if (!array_key_exists('id',$responseArray)) {//if the id supplied is empty 
    $error[] = "No Facebook profile was found matching your credentials.";
  } else {
    $facebook_id = $responseArray['id'];
    $query_get_customer = "
      SELECT * 
      FROM Customer 
      WHERE (facebook_id='$facebook_id')
      ";
    $result_get_customer = mysqli_query($dbc, $query_get_customer);
    $customer_row = mysqli_fetch_array($result_get_customer, MYSQLI_ASSOC);

    if(!array_key_exists('user_id', $customer_row) && !empty($email)) {
      $query_get_customer = "
      SELECT * 
      FROM Customer 
      WHERE (email='$email' AND facebook_id IS NULL)
      ";
      $result_get_customer = mysqli_query($dbc, $query_get_customer);
      $customer_row = mysqli_fetch_array($result_get_customer, MYSQLI_ASSOC);

      if(!array_key_exists('user_id', $customer_row) && !empty($email)) {
        $error[] = "We don't have records of a user matching your facebook profile. You can log in normally and connect your facebook profile to your gottanom profile via settings or register for gottanom with facebook now.";
      } else {
        $query_update_customer = "
        UPDATE Customer 
        SET facebook_id='$facebook_id', facebook_token='$token'
        WHERE (email='$email' AND facebook_id IS NULL)
        ";
        $result_update_customer = mysqli_query($dbc, $query_update_customer);

        $query_get_customer = "
          SELECT * 
          FROM Customer 
          WHERE (facebook_id='$facebook_id')
          ";
        $result_get_customer = mysqli_query($dbc, $query_get_customer);
        $customer_row = mysqli_fetch_array($result_get_customer, MYSQLI_ASSOC);
      }
      
    } elseif(!array_key_exists('user_id', $customer_row) && empty($email)) {
      $error[] = "Your facebook account is not set to provide us with the necessary information. If you have registered, we suggest you signin with your username or email address and your password and connect your account to facebook via your settings. If you have not registered, we recomend that you register now.";
    } 

  }
  
  if(empty($_GET['new_order']))
  {
    $_GET['new_order'] = false;
  }

  if($_GET['new_order']=='true')
  {
    $login_error = 'login_popup_error';
    $login_div = 'login_popup_div';
  }
  else
  {
    $login_error = 'login_error';
    $login_div = 'login_div';
  }
  
  if (empty($error))
  { //if the array is empty , it means no error found

    if (@mysqli_num_rows($result_get_customer) == 1)//if Query is successfull 
    { // A match was made.
      //$_SESSION = mysqli_fetch_array($result_check_credentials, MYSQLI_ASSOC);//Assign the result of this query to SESSION Global Variable
      
      $current_['input'] = $_SESSION['input'];
      
      if($_SESSION['current_address'])
      {
        $current_['on_beach'] = $_SESSION['current_on_beach'];
        $current_['address'] = $_SESSION['current_address'];
        $current_['city'] = $_SESSION['current_city'];
        $current_['state'] = $_SESSION['current_state'];
        $current_['zip'] = $_SESSION['current_zip'];
      
        $_SESSION = $customer_row;//Assign the result of this query to SESSION Global Variable
      
        $_SESSION['current_on_beach'] = $current_['on_beach'];
        $_SESSION['current_address'] = $current_['address'];
        $_SESSION['current_city'] = $current_['city'];
        $_SESSION['current_state'] = $current_['state'];
        $_SESSION['current_zip'] = $current_['zip'];
      }
      else
      {
        $_SESSION = $customer_row;//Assign the result of this query to SESSION Global Variable
      
        $_SESSION['current_on_beach'] = $_SESSION['del_on_beach'];
        $_SESSION['current_address'] = $_SESSION['del_address'];
        $_SESSION['current_city'] = $_SESSION['del_city'];
        $_SESSION['current_state'] = $_SESSION['del_state'];
        $_SESSION['current_zip'] = $_SESSION['del_zip'];
      }
      
      $_SESSION['input'] = $current_['input'];
      
      if($_SESSION['loc_id'])
      {
        if($inputText)
        {
          $inputText .= '&';
        }
        $inputText .= 'loc_id='.$_SESSION['loc_id'];
      }
      
      if($_SESSION['rest_id'])
      {
        if($inputText)
        {
          $inputText .= '&';
        }
        $inputText .= 'rest_id='.$_SESSION['rest_id'];
      }
      
      if($_GET['new_order']=='true')
      {
        $outputtext .= "
          <div>
            <script>
              popup[".'"login"'."].dialog(".'"close"'.");
              executePage(".'"startneworderpopup&loc_id='.$_GET['loc_id'].'&rest_id='.$_GET['rest_id'].'&item_id='.$_GET['item_id'].'"'.");
            </script>
          </div>
          ";
      }
      else
      {
        $outputtext .= "
          <div>
            <script>
              changeURLVariable(".'"'.$inputText.'"'.",{".'"reload"'.":true});
            </script>
          </div>
          ";
      }
    }
    else
    { 
      $outputtext .= "
        <div id='$login_error'>
          We don't have records of a user matching your facebook profile. You can log in normally and connect your facebook profile to your gottanom profile via settings or register for gottanom with facebook now.
        </div>
        <div id='$login_div' scrollTo='true' shake='true' focus='true' />
        ";
    }
  }
  else
  {
    $outputtext .= "
      <div id='$login_error'>
      ";
    foreach ($error as $key => $values)
    { 
      $outputtext .= $values."<br />";
    }
    $outputtext .= "
      </div>
      <div id='$login_div' scrollTo='true' shake='true' focus='true' />
      ";
  }
  
  /// var_dump($error);
  mysqli_close($dbc);
?>