<?php

include_once "connect.php";

require '../vendor/autoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;

$token = $_POST['token'];
FacebookSession::setDefaultApplication('894580503899773', 'b7ea9d916caea560851cf8dff2d195b5');
$session = new FacebookSession($token);
$request = new FacebookRequest($session, 'GET', '/me');
$responseArray = $request->execute()->getGraphObject()->asArray();
//id email first_name gender last_name link locale name timezone updated_time verified

if(array_key_exists('email', $responseArray)) {
  $email = $responseArray['email'];
}

$outputtext .= "
					<div id='register_div' class='register_div' scrollTo='true' flash='true' focus='true'>
						<a name='register_div'></a>
						<div id='register_title' class='register_title content'>
							Register
						</div>
						<form id='user_registration_form' onSubmit='submitForm(this,".'"facebookregisteruser"'."); return false;' method='post' class='updateinfo_form content' autocomplete='off'>
						
							<div id='register_error'></div>

							<input type='hidden' id='facebook_id' name='facebook_id' value='".$responseArray['id']."' size='30' />
							<input type='hidden' id='facebook_token' name='facebook_token' value='".$token."' size='30' />
							
							<table id='registrationtable'>

							<tr>
							<th colspan='2' class='test' id='email_test'><input type='email' id='email' name='email' onkeyup='testEmail(this)' placeholder='Email Address' value='".$email."' size='30' /><div></div></th>
							</tr>

							<tr>
							<th colspan='2' class='test' id='confirm_email_test'><input type='email' id='confirm_email' name='confirm_email' onkeyup='testConfirmEmail(this)' placeholder='Confirm Email' value='".$email."' size='30' /><div></div></th>
							</tr>

							<tr>
							<th colspan='2' class='test' id='phone_number_test'><input type='tel' id='phone_number' name='phone_number' onkeyup='testPhoneNumber(this)' placeholder='Phone Number (xxx-xxx-xxxx)' size='30' /><div></div></th>
							</tr>

							<tr>
							<th colspan='2' class='test' id='first_name_test'><input type='text' id='first_name' name='first_name' onkeyup='testFirstName(this)' placeholder='First Name' value='".$responseArray['first_name']."' size='30' /><div></div></th>
							</tr>

							<tr>
							<th colspan='2' class='test' id='last_name_test'><input type='text' id='last_name' name='last_name' onkeyup='testLastName(this)' placeholder='Last Name' value='".$responseArray['last_name']."' size='30' /><div></div></th>
							</tr>

							<tr>
							<th colspan='2' class='test' id='terms_of_use_test'><input type='checkbox' onClick='testTermsOfUse(this)' id='terms_of_use' name='terms_of_use'>I agree with the <a target='_blank' href='http://gottanom.com/termsofuse.php'><b>terms of use</b></a>.</th>
							</tr>

							<tr>
							<th colspan='2' class='center'><input type='submit' id='stylebutton' value='Facebook Register' title='Facebook Register' /></th>
							</tr>

							</table>

						</form>
						<script>
							testForm('#user_registration_form');
							checkForm('#user_registration_form');
						</script>
					</div>
					";
					
	mysqli_close($dbc);
?>