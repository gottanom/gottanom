<?php

if(empty($_SESSION))
{
	session_start();
}

/*
$outputtext = "before";

if(empty($_POST))
{
	$outputtext .= "empty";
}
else
{
	foreach($_POST as $key=>$result) {
		$outputtext .= $key.":".$result.' , ';
	}
}

if(!empty($_REQUEST))
{
	$outputtext .= "not empty";
}

if(empty($_POST) && !empty($_REQUEST))
{
	$outputtext .= "both";
	foreach($_REQUEST as $key=>$value)
	{
		$outputtext .= "key:$key";
		$_POST[$key] = $value;
	}
}
else
{
	$outputtext .= "not both";
}

$outputtext .= "after";
*/

if(!empty($dbc)) {
	$stat = $dbc->stat();
} else {
	$stat = null;
}

if(empty($stat))
{
	//Define constant to connect to database
	DEFINE('DATABASE_USER', 'INOMREADONLY');
	DEFINE('DATABASE_PASSWORD', 'iN0md@tc0m');
	DEFINE('DATABASE_HOST', 'INOMDB.db.11450909.hostedresource.com');
	//DEFINE('DATABASE_HOST', '127.0.0.1');
	DEFINE('DATABASE_NAME', 'INOMDB');
	//Default time zone ,to be able to send mail
	date_default_timezone_set('US/Eastern');

	//You might not need this
	ini_set('SMTP', "mail.myt.mu"); // Overide The Default Php.ini settings for sending mail


	//This is the address that will appear coming from ( Sender )
	define('EMAIL', 'noreply@gottanom.com');

	//Define the root url where the script will be found such as http://website.com or http://website.com/Folder/
	//DEFINE('WEBSITE_URL', 'http://gottanom.com');
	DEFINE('WEBSITE_URL', '127.0.0.1');


	// Make the connection:
	$dbc = @mysqli_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME);

	if (!$dbc)
	{
	    trigger_error('Could not connect to MySQL: ' . mysqli_connect_error());
	    //echo mysqli_connect_error ( void );
	}

	include 'sanitize.php';

}

?>