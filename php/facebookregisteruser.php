<?php
	include ('connecttowrite.php');

    $error = array();//Declare An Array to store any error message
    
    if (!$_POST['terms_of_use'])
    {
    	$error[] = 'You must agree to the terms of use.';
    }

		if (!$_POST['facebook_token'] || !$_POST['facebook_id'])
		{
			$error[] = 'It seems your facebook information is incorrect or incomplete.';
		} else {
			$facebook_token = $_POST['facebook_token'];
			$facebook_id = $_POST['facebook_id'];

			$query_verify_facebook = "
				SELECT * FROM Customer 
				WHERE facebook_token ='$facebook_token' OR facebook_id ='$facebook_id'
				";
			$result_verify_facebook = mysqli_query($dbc, $query_verify_facebook);
			if (!$result_verify_facebook)
			{//if the Query Failed ,similar to if($result_verify_email==false)
				$error[] = ' A database error occured. ';
			}

			if (mysqli_num_rows($result_verify_facebook) > 0)
			{
				$error[] = 'Your facebok account is already registered. You can use facebook to login with this account.';
			}
		}

    if (empty($_POST['email'])) {
        $error[] = 'Please enter your email address. ';
    }
    elseif (!preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $_POST['email']))
    {
    	$error[] = 'Your email address is invalid.  ';
    }
    elseif ($_POST['email'] != $_POST['confirm_email'])
    {
    	$error[] = "Your email addresses don't match. ";
    }
    else
    {
			//regular expression for email validation
			$newemail = $_POST['email'];
			// Make sure the email address is available:
			$query_verify_email = "
				SELECT * FROM Customer 
				WHERE email ='$newemail'
				";
			$result_verify_email = mysqli_query($dbc, $query_verify_email);
			if (!$result_verify_email)
			{//if the Query Failed ,similar to if($result_verify_email==false)
				$error[] = ' A database error occured. ';
			}

			if (mysqli_num_rows($result_verify_email) > 0)
			{
				$error[] = 'Your email address is already registered. You can use facebook to login with this account.';
			}
		}

		if (empty($_POST['phone_number']))
		{
		    $error[] = 'Please enter your phone number. ';
		}
		elseif (preg_match("/^([1]-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/i", $_POST['phone_number']))
		{
			//regular expression for email validation
			$newphonenumber = $_POST['phone_number'];
		}
		else
		{
			 $error[] = 'Your phone number is invalid. ';
		}



		$newpassword = substr(md5(uniqid(rand(), true)),0,16);
		$salt = md5(uniqid(rand(), true));
		$iterations = 10;
		$hash = crypt($newpassword,$salt);
		for ($i = 0; $i < $iterations; ++$i)
		{
		$hash = crypt($hash . $newpassword,$salt);
		}


		if (empty($error)) //send to Database if there's no error '
		{ // If everything's OK...
			
			if($_POST['first_name'])
			{
				$newfirstname = $_POST['first_name'];
			}
			else
			{
				$newfirstname = NULL;
			}
			
			if($_POST['last_name'])
			{
				$newlastname = $_POST['last_name'];
			}
			else
			{
				$newlastname = NULL;
			}
			
			$date = date("Y-m-d H:i:s");

			$query_insert_user = "
				INSERT INTO Customer 
						(password, salt, facebook_token, facebook_id,email, first_name, last_name, phone, date) 
				VALUES ('$hash', '$salt', '$facebook_token', '$facebook_id', '$newemail', '$newfirstname', '$newlastname', '$newphonenumber', '$date')
				";
			
			$result_insert_user = mysqli_query($dbc, $query_insert_user);
			
			if (!$result_insert_user)
			{
				$error[] = 'The attempt to add you to the database failed. ';
			}

			if (mysqli_affected_rows($dbc) == 1)
			{ //If the Insert Query was successfull.
				// Send the email:
				$message = "To activate your account, please click or copy the link below.\n\n";
				$message .= WEBSITE_URL . '/activate.php?email=' . urlencode($newemail) . "&key=$activation";
				$mailreturn = mail($newemail, 'Registration Confirmation', $message, 'From: noreply@gottanom.com');
				
				// Flush the buffered output.

				// Finish the page:
				$error[] = 'Thank you for registering! You may now sign in via facebook.';
				$error[] = '
					<script>
						$("#userregform").trigger("reset");
					</script>
					';
				
				//$error[] = '<br><br><input type="submit" onClick="$( this ).dialog('."'close'".');" id="stylebutton" value="Thanks" title="Thanks" />';
			}
			else
			{ // If it did not run OK.
				$error[] = 'You could not be registered due to a system error. We apologize for any inconvenience.';
			}

    }
	
	$outputtext .= "<div id='register_error'>";
	
	foreach ($error as $key => $values)
	{	
		$outputtext .= $values."<br>";
	}
	
	$outputtext .= "
					</div>
					<div id='register_div' scrollTo='true' shake='true' focus='true' />
				";
  
    mysqli_close($dbc);//Close the DB Connection


?>