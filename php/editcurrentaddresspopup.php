<?php

include "stateslist.php";

set_time_limit(0);
ignore_user_abort(1);

session_start();

$outputtext .= "
<div id='main_left_content'>
<center>
<form id='current_address_form' onSubmit='setAddress(this);return false;' method='post' class='login_form'>

	<table id='registrationtable'>

	<tr id='delivery_preference_test'>
	<th colspan='1' class='optional'><input type='radio' id='delivery_preference1' name='delivery_preference' value='delivery' onchange='' placeholder='Delivery' size='30' ";

	if($_SESSION['delivery_preference']=='delivery'||!$_SESSION['delivery_preference'])
	{
		$outputtext .= "checked";
	}

	$outputtext .= " /><label for='delivery_preference1'>Delivery</label></th>
	<th colspan='1' class='optional'><input type='radio' id='delivery_preference2' name='delivery_preference' value='pickup' onchange='' placeholder='Pick Up' size='30' ";

	if($_SESSION['delivery_preference']=='pickup')
	{
		$outputtext .= "checked";
	}

	$outputtext .= " /><label for='delivery_preference2'>Pick Up</label></th>
	</tr>

	<tr>
	<th colspan='2' class='optional' id='current_on_beach_test'><input type='checkbox' id='current_on_beach' name='current_on_beach' onchange='testOnBeach(this)' placeholder='On The Beach' size='30' ".$_SESSION['del_on_beach']." /><label for='current_on_beach'>On The Beach</label></th>
	</tr>

	<tr>
	<th colspan='2' class='optional' id='current_address_test'><input type='text' id='current_address' name='current_address' onchange='testAddress(this)' onkeyup='testAddress(this)' placeholder='Address' value='".($_SESSION['current_address']?$_SESSION['current_address']." ".($_SESSION['current_apt']?$_SESSION['current_apt']." ":"").$_SESSION['current_city'].", ".$_SESSION['current_state']." ".$_SESSION['current_zip']:"")."' size='30' /></th>
	</tr>
	
	<tr>
	<th colspan='2' id='registerbuttontest' class='optional'>&nbsp;</th>
	</tr>

	<tr>
	<th colspan='2' class='center'><input type='submit' id='registerbutton' class='registerbutton' value='Save Changes' title='Save Changes' /></th>
	</tr>
	
	<tr>
	<th colspan='1' class='center'><input type='button' class='registerbutton' onclick='executePage(".'"leftcategories"'.")' value='Cancel' title='Cancel' /></th>
	<th colspan='1' class='center'><input type='button' id='registerbutton' class='registerbutton' onclick='executePage(".'"removeaddress"'.")' value='Delete' title='Delete' /></th>
	</tr>
	
	</table>
	
</form>
</center>
<script>
	testForm('#current_address_form');
	checkForm('#current_address_form');
	$(function() {
		$( '#delivery_preference_test' ).buttonset();
	});
	positionLeft();
</script>
</div>
";

?>