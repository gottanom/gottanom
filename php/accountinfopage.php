<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";
	
	include 'accountinfomenu.php';

	$outputtext .= "
	<div id='main_right' class='col span_3_of_4'>
		<div id='top'>
				<h1>".($_SESSION['first_name']?htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s":"Your")." Information</h1>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
				<form id='account_info_form' onSubmit='submitForm(this,".'"updateinfo"'.");return false' method='post' class='updateinfo_form' autocomplete='off'>
	
					<table id='registrationtable'>
					
					<tr>
					<th colspan='2' class='center' >Account Information</th>
					</tr>
					".($_SESSION['username']?"
					<tr>
					<td colspan='2' class='center' >Username: ".$_SESSION['username']."</td>
					</tr>
					":"")."
					<tr>
					<th colspan='2' class='optional' id='email_test'><input type='email' id='email' name='email' onkeyup='testEmail(this,{".'"required"'.":false})' placeholder='";
					
	if($_SESSION['email'])
	{
		$outputtext .= htmlspecialchars($_SESSION['email'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "Email Address";
	}
					
	$outputtext .= "' size='30' /></th>
					</tr>
	
					<tr>
					<th colspan='2' class='optional' id='confirm_email_test'><input type='email' id='confirm_email' name='confirm_email' onkeyup='testConfirmEmail(this,{".'"required"'.":false})' placeholder='Confirm Email' size='30' /></th>
					</tr>
	
					<tr>
					<th colspan='2' class='optional' id='phone_number_test'><input type='tel' id='phone_number' name='phone_number' onkeyup='testPhoneNumber(this,{".'"required"'.":false})' placeholder='";
					
	if($_SESSION['phone'])
	{
		$outputtext .= htmlspecialchars($_SESSION['phone'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "XXX-XXX-XXXX";
	}
					
	$outputtext .= "' size='30' /></th>
					</tr>
	
					<tr>
					<th colspan='2' class='optional' id='first_name_test'><input type='text' id='first_name' name='first_name' onkeyup='testFirstName(this,{".'"required"'.":false})' placeholder='";
					
	if($_SESSION['first_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['first_name'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "First Name";
	}
					
	$outputtext .= "' size='30' /></th>
					</tr>
	
					<tr>
					<th colspan='2' class='optional' id='last_name_test'><input type='text' id='last_name' name='last_name' onkeyup='testLastName(this,{".'"required"'.":false})' placeholder='";
					
	if($_SESSION['last_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['last_name'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "Last Name";
	}
					
	$outputtext .= "' size='30' /></th>
					</tr>
					
					<tr>
					<th colspan='2' class='optional' id='delivery_preference_test'>
						<select type='text' id='delivery_preference' name='delivery_preference' onchange='' >
					";
	
	if($_SESSION['delivery_preference']=='delivery')
	{
		$outputtext .= "<option selected='selected' value='delivery'>Delivery</option>";
		$outputtext .= "<option value='pickup'>Pick Up</option>";
	}
	else
	{
		$outputtext .= "<option value='delivery'>Delivery</option>";
		$outputtext .= "<option selected='selected' value='pickup'>Pick Up</option>";
	}

	$outputtext .= "	</select>
					</th>
					</tr>
	
					<tr>
					<th colspan='2' id='registerbutton_test' class='center'><input type='submit' id='registerbutton' value='Update Info' title='Disabled' disabled='true' /></th>
					</tr>
	
					</table>
	
				</form>
			<center>
			<script>
				testForm('#account_info_form');
				checkForm('#account_info_form');
			</script>
		</div>
	</div>
	";
	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    //echo $outputtext;
?>