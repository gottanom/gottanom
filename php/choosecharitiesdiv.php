<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";
	
	//include 'accountinfomenu.php';
	
	$outputtext .= "
	<div id='main_right' class='col span_3_of_4'>
			<div id='top'>
				<h1>
					Choose Your Charities
				</h1>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
				<form id='choose_charity_form' onSubmit='submitForm(this,".'"updateinfo"'.");return false' method='post' class='updateinfo_form' autocomplete='off'>
	
					<table id='registrationtable' class='choose_charity_table'>
					
					<tr>
					<th colspan='2' class='center' >Charities</th>
					</tr>
					
					<tr>
					<td colspan='2' class='center' >Selecting none will result in donations being split amongst all.</td>
					</tr>
					";

	$user_id = $_SESSION['user_id'];
	$query_get_charity = "
		SELECT * FROM Customer_Charity 
		WHERE user_id = '$user_id'
		";
	$result_get_charity = mysqli_query($dbc, $query_get_charity);
	$customer_charity_info = array();

	if (@mysqli_num_rows($result_get_charity) > 0)//if Query is successfull 
	{ // A match was made.
		while($customer_charity_row = mysqli_fetch_array($result_get_charity, MYSQLI_ASSOC))
		{
			$customer_charity_info[$customer_charity_row['charity_id']] = $customer_charity_row['donate'];
		}
	}
	
	$loc_id = $_SESSION['loc_id'];
	$query_get_charity = "
	SELECT * FROM Charity 
	WHERE loc_id = '$loc_id'
	";
	$result_get_charity = mysqli_query($dbc, $query_get_charity);
	while($charity_row = mysqli_fetch_array($result_get_charity, MYSQLI_ASSOC))
	{
		$outputtext .= "
				<tr>
				<th colspan='2' class='left'><input type='checkbox' id='".$charity_row['charity_id']."' value='".$charity_row['charity_id']."' name='charity' size='30' ";
				
		if($customer_charity_info[$charity_row['charity_id']])
		{
			$outputtext .= "checked='checked' ";
		}
				
		$outputtext .= "/><label for='".$charity_row['charity_id']."'>".$charity_row['charity_name']."</label></th>
				</tr>
			";
	}
					
	$outputtext .= "
					</table>

					<table id='registrationtable'>

					<tr>
					<th colspan='2' id='registerbutton_test' class='center'><input type='submit' id='registerbutton' value='Update Info' title='Disabled' disabled='true' /></th>
					</tr>

					</table>
	
				</form>
			<center>
			<script>
				testForm('#choose_charity_form');
				checkForm('#choose_charity_form');
				$(function() {
					$( '.choose_charity_table' ).buttonset();
				});
			</script>
		</div>
	</div>
	";
	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    //echo $outputtext;
?>