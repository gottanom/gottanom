<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";
	
	//include 'accountinfomenu.php';

	$outputtext .= "
	<div id='main_right' class='col span_3_of_4'>
		<div id='top'>
			<h1>
				Your Delivery Address
			</h1>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
			<div id='main_left_content'>
				<form id='current_address_form' onSubmit='setAddress(this);return false;' method='post' class='login_form'>

					<table id='registrationtable'>
			
					<tr>
					<th colspan='2' class='optional' id='current_on_beach_test'><input type='checkbox' id='del_on_beach' name='current_on_beach' onchange='testOnBeach(this)' placeholder='On The Beach' size='30' ".$_SESSION['del_on_beach']." /><label for='del_on_beach'>On The Beach</label></th>
					</tr>

					<tr>
					<th colspan='2' class='optional' id='del_address_test'><input type='text' id='del_address' name='del_address' onchange='testAddress(this)' onkeyup='testAddress(this)' placeholder='Address' value='".($_SESSION['del_address']?$_SESSION['del_address']." ".($_SESSION['del_apt']?$_SESSION['del_apt']." ":"").$_SESSION['del_city'].", ".$_SESSION['del_state']." ".$_SESSION['del_zip']:"")."' size='30' /></th>
					</tr>

					<tr>
					<th colspan='2' class='center'><input type='submit' id='stylebutton' value='Enter Address' title='Enter Address' /></th>
					</tr>

					</table>

				</form>
			</div>
			<script>
				testForm('#current_address_form');
				checkForm('#current_address_form');
				$(function() {
					$( '#delivery_preference_test' ).buttonset();
				});
			</script>
		</div>
	</div>
	";
	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    //echo $outputtext;
?>