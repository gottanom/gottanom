<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";
	
	$outputtext .= "

	<div id='mainleft'>
		<div id='main_left_content'>
		<center>
			<h2>".($_SESSION['first_name']?htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s":"Your")." Information</h2>
			<br/>
			<a onclick='executePage(".'"accountinfopage"'.")'>Account Information</a>
			<br/>
			<a onclick='executePage(".'"deliveryaddressdiv"'.")'>Delivery Address</a>
			<br/>
			<a onclick='executePage(".'"choosecharitiesdiv"'.")'>Charities</a>
			<br/>
			<a onclick='executePage(".'"changepassworddiv"'.")'>Change Password</a>
		</center>
		</div>
	</div>

		<div id='top'>
			<center>
				<h1>
					";
					
	if($_SESSION['first_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s";
	}
	else
	{
		$outputtext .= "Your";
	}
	
	$outputtext .= " Information
				</h1>
			</center>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
			<center>
				<form id='change_password_form' onSubmit='submitForm(this,".'"updatepassword.php"'.");return false' method='post' class='updateinfo_form' autocomplete='off'>
	
					<table id='registrationtable'>
					
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='center' >New Password</th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='passwordtest'>Password</th>
					<th colspan='2' class='center'><input type='password' id='newpassword' name='newpassword' onkeyup='testPasswordUpdate()' placeholder='Password' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='confirmpasswordtest'>Confirm Password</th>
					<th colspan='2' class='center'><input type='password' id='confirmpassword' name='confirmpassword' onkeyup='testConfirmPasswordUpdate()' placeholder='Confirm Password' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
					
					<tr>
					<th colspan='6' class='center'><center><hr></center></th>
					</tr>
					
					</table>
					";
					
	$outputtext .= "
					<table id='registrationtable'>
					
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='test' id='currentpasswordtest'>Current Password</th>
					<th colspan='2' class='center'><input type='password' id='currentpassword' name='currentpassword' onkeyup='testPasswordCurrent()' placeholder='Current Password' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='center'><input type='submit' id='registerbutton' value='Update Info' title='Disabled' disabled='true' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					</table>
	
				</form>
			<center>
			<script>
				testForm('#change_password_form');
				checkForm('#change_password_form');
			</script>
		</center>
		</div>
	";
	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    //echo $outputtext;
?>