<?php

session_start();

foreach($_POST as $key => $value)
{
	if($value=='true')
	{
		$_SESSION[$key] = 'checked';
		//$outputtext .= '<javascript>alert("checked");</javascript>';
	}
	elseif($value=='false')
	{
		$_SESSION[$key] = '';
		//$outputtext .= '<javascript>alert("checked");</javascript>';
	}
	else
	{
		$_SESSION[$key] = $value;
	}
	
	//$outputtext .= '<variable name="'.$key.'" value="'.$_SESSION[$key].'">';
	$outputtext .= '<variable name="'.$key.'">'.$_SESSION[$key].'</variable>';
}

include ("connecttowrite.php");

//$API_KEY = "AIzaSyDBFhPAEd2JZIY5utxpYt8cS2Q9oM0QBmc";
$API_KEY = "AIzaSyAsQn6WV_46ndNtph-RYrxJCIaHOtUTsno";
$origin = str_replace(" ", "+", $_POST['current_address']." ".$_POST['current_city']." ".$_POST['current_state']." ".$_POST['current_zip']);
$forbidden = array(
        "@", "$", "%", "^", "*", "!", "|", ",", "`", "~","#",
        "<", ">",
        "{", "}",
        "[", "]",
        "(", ")"
    );
foreach($forbidden as $char ) {
	$origin = str_replace($char, "", $origin);
}

//$outputtext .= " origin ".$origin;

// add destinations from database
$destinations = "";
$query_get_destinations = "SELECT location_address FROM Location WHERE confirmed=1";
$result_get_destinations = mysqli_query($dbc, $query_get_destinations);
while($destinations_row = mysqli_fetch_array($result_get_destinations, MYSQLI_ASSOC)) {
	if($destinations) {
		$destinations .= "|";
	}
	$destinations .= $destinations_row['location_address'];
}

//$outputtext .= " destinations ".$destinations;

$destinations = str_replace(",","",str_replace(" ", "+", $destinations));
//$destinations = urlencode($destinations);

//$outputtext .= " destinations ".$destinations;

$url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$origin.'&destinations='.$destinations.'&mode=driving&units=imperial&language=en-EN&key='.$API_KEY;

//$outputtext .= "done ";
//$outputtext .= $url;

$ch = curl_init();
$curlConfig = array(
    CURLOPT_URL            => $url,
    CURLOPT_RETURNTRANSFER => true
);
curl_setopt_array($ch, $curlConfig);
$resultOut = curl_exec($ch);
$outputtext .= $resultOut;
$result = json_decode(curl_exec($ch),true);
curl_close($ch);

$destination = $result['destination_addresses'][0];
$distance = $result['rows'][0]['elements'][0]['distance']['value'];
$distanceText = $result['rows'][0]['elements'][0]['distance']['text'];

//$outputtext .= " length ".count($result['destination_addresses']);
//$outputtext .= " distance: ".$distance;


for($i = 1; $i < count($result['destination_addresses']); $i++) {
	//$outputtext .= " dist: ".$restuls['rows']['elements'][$i]['distance']['value'];

	if($result['rows'][0]['elements'][$i]['distance']['value'] < $distance) {
		$distance = $result['rows'][0]['elements'][$i]['distance']['value'];
		$distanceText = $result['rows'][0]['elements'][$i]['distance']['text'];
		$destination = $result['destination_addresses'][$i];
	}
}

//$outputtext .= " destination ".$destination;

$query_get_location = "SELECT location, loc_id FROM Location WHERE location_address='".$destination."'";
$result_get_location = mysqli_query($dbc, $query_get_location);
$location_row = mysqli_fetch_array($result_get_location, MYSQLI_ASSOC);
$_SESSION['loc_id'] = $location_row['loc_id'];

if(array_key_exists('user_id',$_SESSION))
{
	$query_update_customer = "UPDATE Customer SET ";
	//del_address='$newaddress'
	$result_update_customer = mysqli_query($dbc, $query_update_customer);
	
	$variable_names = array("on_beach","address","apt","city","state","zip");

	foreach($variable_names as $value) {
		if(!$_POST['del_address'])
		{
			if($query_update_customer != "UPDATE Customer SET ")
			{
				$query_update_customer .= ", ";
			}
			
			$query_update_customer .= "del_".$value."='".$_SESSION['current_'.$value]."' ";
			$_SESSION['del_'.$value] = $_SESSION['current_'.$value];
			$outputtext .= '<variable name="del_'.$value.'">'.$_SESSION['current_'.$value].'</variable>';
		} else {
			$_SESSION['current_'.$value] = $_SESSION['del_'.$value];

			if($query_update_customer != "UPDATE Customer SET ")
			{
				$query_update_customer .= ", ";
			}
			
			$query_update_customer .= "del_".$value."='".$_SESSION['del_'.$value]."'";
			$_SESSION['del_'.$value] = $_SESSION['del_'.$value];
			$outputtext .= '<variable name="del_'.$value.'">'.$_SESSION['del_'.$value].'</variable>';
		}
	}
	
	$query_update_customer .= ",
		loc_id='".$_SESSION['loc_id']."'"."
		WHERE user_id=".$_SESSION['user_id']."
		";
	$result_update_customer = mysqli_query($dbc, $query_update_customer);
	
	//$outputtext .= $query_update_customer;
	//$outputtext .= $dbc;

}

//$outputtext .= "result: ".$resultOut;

if(!$_POST['del_address']) {
	if($distance > 32186) {
		$outputtext .= "
			<div type='popup' name='update_info' title='Update Message'>
				<button>
					{ text: 'Use This Location', click: function() { location.reload(); } }, { text: 'No Thanks', click: function() { $( this ).dialog( 'close' ); } }
				</button>
				<onClose>
					executePage('removeaddress');
				</onClose>
				<text>
					We're not in your area, yet! But our ".$location_row['location']." restaurants are the closest at ".$distanceText." away.
				</text>
			</div>
			";
	} else {
		$outputtext .= "
			<div>
				<script>
					changeURLVariable(".'"'."loc_id=".$_SESSION['loc_id'].'"'.",{".'"reload"'.":true});
				</script>
			</div>
			";
	}
} else {
	$outputtext .= "
		<div>
			<script>
				changeURLVariable(".'"'."loc_id=".$_SESSION['loc_id'].'"'.");
			</script>
		</div>

		<div type='popup' name='update_info' title='Update Message'>
			<button>
				{ text: 'Thanks', click: function() { $( this ).dialog( 'close' ); } }
			</button>
			<text>
				Your delivery address has been updated.
			</text>
		</div>
		";
}

mysqli_close($dbc);

echo $outputtext;

?>