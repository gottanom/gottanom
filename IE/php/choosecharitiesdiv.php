<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";
	
	$outputtext .= "

	<div id='mainleft'>
		<div id='main_left_content'>
		<center>
			<h2>".($_SESSION['first_name']?htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s":"Your")." Information</h2>
			<br/>
			<a onclick='executePage(".'"accountinfopage"'.")'>Account Information</a>
			<br/>
			<a onclick='executePage(".'"deliveryaddressdiv"'.")'>Delivery Address</a>
			<br/>
			<a onclick='executePage(".'"choosecharitiesdiv"'.")'>Charities</a>
			<br/>
			<a onclick='executePage(".'"changepassworddiv"'.")'>Change Password</a>
		</center>
		</div>
	</div>

		<div id='top'>
			<center>
				<h1>
					";
					
	if($_SESSION['first_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s";
	}
	else
	{
		$outputtext .= "Your";
	}
	
	$outputtext .= " Information
				</h1>
			</center>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
			<center>
				<form id='choose_charity_form' onSubmit='submitForm(this,".'"updateinfo.php"'.");return false' method='post' class='updateinfo_form' autocomplete='off'>
	
					<table id='registrationtable' class='choose_charity_table'>
					
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='center' >Charities</th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
					
					<tr>
					<td colspan='1' class='center'>&nbsp;</td>
					<td colspan='4' class='center' >Selecting none will result in donations being split amongst all.</td>
					<td colspan='1' class='center'>&nbsp;</td>
					</tr>
					";

	$user_id = $_SESSION['user_id'];
	$query_get_charity = "
		SELECT * FROM Customer_Charity 
		WHERE user_id = '$user_id'
		";
	$result_get_charity = mysqli_query($dbc, $query_get_charity);
	$customer_charity_info = array();

	if (@mysqli_num_rows($result_get_charity) > 0)//if Query is successfull 
	{ // A match was made.
		while($customer_charity_row = mysqli_fetch_array($result_get_charity, MYSQLI_ASSOC))
		{
			$customer_charity_info[$customer_charity_row['charity_id']] = $customer_charity_row['donate'];
		}
	}
	
	$loc_id = $_SESSION['loc_id'];
	$query_get_charity = "
	SELECT * FROM Charity 
	WHERE loc_id = '$loc_id'
	";
	$result_get_charity = mysqli_query($dbc, $query_get_charity);
	while($charity_row = mysqli_fetch_array($result_get_charity, MYSQLI_ASSOC))
	{
		$outputtext .= "
				<tr>
				<th colspan='1' class='center'>&nbsp;</th>
				<th colspan='2' class='optional' id='charitytest'>".$charity_row['charity_name']."</th>
				<th colspan='2' class='center'><input type='checkbox' id='".$charity_row['charity_id']."' value='".$charity_row['charity_id']."' name='charity' size='30' ";
				
		if($customer_charity_info[$charity_row['charity_id']])
		{
			$outputtext .= "checked='checked' ";
		}
				
		$outputtext .= "/></th>
				<th colspan='1' class='center'>&nbsp;</th>
				</tr>
			";
	}
					
	$outputtext .= "
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='center'><input type='submit' id='registerbutton' value='Update Info' title='Update Info' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>

					</table>
	
				</form>
			<center>
			<script>
				testForm('#choose_charity_form');
				checkForm('#choose_charity_form');
			</script>
		</center>
		</div>
	";
	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    //echo $outputtext;
?>