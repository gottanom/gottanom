<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";
	
	$outputtext .= "

	<div id='mainleft'>
		<div id='main_left_content'>
		<center>
			<h2>".($_SESSION['first_name']?htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s":"Your")." Information</h2>
			<br/>
			<a onclick='executePage(".'"accountinfopage"'.")'>Account Information</a>
			<br/>
			<a onclick='executePage(".'"deliveryaddressdiv"'.")'>Delivery Address</a>
			<br/>
			<a onclick='executePage(".'"choosecharitiesdiv"'.")'>Charities</a>
			<br/>
			<a onclick='executePage(".'"changepassworddiv"'.")'>Change Password</a>
		</center>
		</div>
	</div>

		<div id='top'>
			<center>
				<h1>
					";
					
	if($_SESSION['first_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s";
	}
	else
	{
		$outputtext .= "Your";
	}
	
	$outputtext .= " Information
				</h1>
			</center>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
		<center>
			<div id='main_left_content'>
				<form id='current_address_form' onSubmit='submitForm(this,".'"setcurrentaddress.php"'.");return false;' method='post' class='login_form'>

					<table id='registrationtable'>
					
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='center' >Delivery Address</th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>

					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='optional' id='del_on_beach_test'><input type='checkbox' id='del_on_beach' name='del_on_beach' onchange='testOnBeach(this)' placeholder='On The Beach' size='30' ".$_SESSION['del_on_beach']." /><label for='del_on_beach'>On The Beach</label></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='del_address_test'>Street Address</th>
					<th colspan='2' class='center'><input type='text' id='del_address' name='del_address' onkeyup='testAddressUpdate()' placeholder=".'"';
					
	if($_SESSION['del_address'])
	{
		$outputtext .= htmlspecialchars($_SESSION['del_address'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "Street Address";
	}
					
	$outputtext .= '"'." size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='del_city_test'>City</th>
					<th colspan='2' class='center'><input type='text' id='del_city' name='del_city' onkeyup='testCityUpdate()' placeholder='";
					
	if($_SESSION['del_city'])
	{
		$outputtext .= htmlspecialchars($_SESSION['del_city'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "City";
	}
					
	$outputtext .= "' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='del_state_test'>State</th>
					<th colspan='2' class='center'>
		";
	
	$states = statesList();
	$stateletters = $_SESSION['del_state'];
	
	if($stateletters)
	{
		$statename = $states[$stateletters];
	}
	else
	{
		$statename = "Select Your State...";
	}

	$outputtext .= "<select type='text' id='del_state' name='del_state' onchange='testStateUpdate()' >";
	$outputtext .= "<option selected='selected' value='".$stateletters."'>".$statename."</option>";
					
	$outputtext .= "</option>";

	foreach($states as $key=>$value)
	{
		$outputtext .= "<option value='".$key."'>".$value."</option>";
	}

	$outputtext .= "</select>";

	//<input type='text' id='newstate' name='newstate' onkeyup='testState()' placeholder='Virginia' size='30' />
	
	$outputtext .= "
					</th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='del_zip_test'>Zip Code</th>
					<th colspan='2' class='center'><input type='text' id='del_zip' name='del_zip' onkeyup='testZipCodeUpdate()' placeholder='";
					
	if($_SESSION['del_zip'])
	{
		$outputtext .= htmlspecialchars($_SESSION['del_zip'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "Zip Code";
	}
					
	$outputtext .= "' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>

					<tr>
					<th colspan='6' class='center'><input type='submit' id='stylebutton' value='Enter Address' title='Enter Address' /></th>
					</tr>

					</table>

				</form>
			</div>
			<script>
				testForm('#current_address_form');
				checkForm('#current_address_form');
				$(function() {
					$( '#delivery_preference_test' ).buttonset();
				});
			</script>
		</center>
		</div>
	";
	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    echo $outputtext;
?>