<?php

	session_start();
	
	include "connect.php";
	
	include "stateslist.php";
	
	$outputtext .= "

	<div id='mainleft'>
		<div id='main_left_content'>
		<center>
			<h2>".($_SESSION['first_name']?htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s":"Your")." Information</h2>
			<br/>
			<a onclick='executePage(".'"accountinfopage"'.")'>Account Information</a>
			<br/>
			<a onclick='executePage(".'"deliveryaddressdiv"'.")'>Delivery Address</a>
			<br/>
			<a onclick='executePage(".'"choosecharitiesdiv"'.")'>Charities</a>
			<br/>
			<a onclick='executePage(".'"changepassworddiv"'.")'>Change Password</a>
		</center>
		</div>
	</div>

		<div id='top'>
			<center>
				<h1>
					";
					
	if($_SESSION['first_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['first_name'], ENT_QUOTES)."'s";
	}
	else
	{
		$outputtext .= "Your";
	}
	
	$outputtext .= " Information
				</h1>
			</center>
		</div>
		";
		
	$outputtext .= "
		<div id='main'>
			<center>	
				<form onSubmit='submitForm(this,".'"updateinfo.php"'.");return false' method='post' class='updateinfo_form' autocomplete='off'>
	
					<table id='registrationtable'>
					
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='center' >Account Information</th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>

					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<td colspan='4' class='center' >Username: ".$_SESSION['username']."</td>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='newemail_test'>Email Address</th>
					<th colspan='2' class='center'><input type='email' id='newemail' name='newemail' onkeyup='testEmailUpdate()' placeholder='";
					
	if($_SESSION['email'])
	{
		$outputtext .= htmlspecialchars($_SESSION['email'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "Email Address";
	}
					
	$outputtext .= "' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='confirmemail_test'>Confirm Email</th>
					<th colspan='2' class='center'><input type='email' id='confirmemail' name='confirmemail' onkeyup='testConfirmEmailUpdate()' placeholder='Confirm Email' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='newphonenumber_test'>Phone Number</th>
					<th colspan='2' class='center'><input type='tel' id='newphonenumber' name='newphonenumber' onkeyup='testPhoneNumberUpdate()' placeholder='";
					
	if($_SESSION['phone'])
	{
		$outputtext .= htmlspecialchars($_SESSION['phone'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "XXX-XXX-XXXX";
	}
					
	$outputtext .= "' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='newfirstname_test'>First Name</th>
					<th colspan='2' class='center'><input type='text' id='newfirstname' name='newfirstname' onkeyup='testFirstNameUpdate()' placeholder='";
					
	if($_SESSION['first_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['first_name'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "First Name";
	}
					
	$outputtext .= "' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='newlastname_test'>Last Name</th>
					<th colspan='2' class='center'><input type='text' id='newlastname' name='newlastname' onkeyup='testLastNameUpdate()' placeholder='";
					
	if($_SESSION['last_name'])
	{
		$outputtext .= htmlspecialchars($_SESSION['last_name'], ENT_QUOTES);
	}
	else
	{
		$outputtext .= "Last Name";
	}
					
	$outputtext .= "' size='30' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
					
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='2' class='optional' id='newdeliverypreference_test'>Delivery Preference</th>
					<th colspan='2' class='center'>
						<select type='text' id='newdeliverypreference' name='newdeliverypreference' onchange='' >
					";
	
	if($_SESSION['delivery_preference']=='delivery')
	{
		$outputtext .= "<option selected='selected' value='delivery'>Delivery</option>";
		$outputtext .= "<option value='pickup'>Pick Up</option>";
	}
	else
	{
		$outputtext .= "<option value='delivery'>Delivery</option>";
		$outputtext .= "<option selected='selected' value='pickup'>Pick Up</option>";
	}

	$outputtext .= "	</select>
					</th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
					";
					
	$outputtext .= "
					<tr>
					<th colspan='1' class='center'>&nbsp;</th>
					<th colspan='4' class='center'><input type='submit' id='registerbutton' value='Update Info' title='Update Info' /></th>
					<th colspan='1' class='center'>&nbsp;</th>
					</tr>
	
					</table>
	
				</form>
			<center>
		</div>
		";


	
    /// var_dump($error);
    // mysqli_close($dbc);
    
    echo $outputtext;
?>